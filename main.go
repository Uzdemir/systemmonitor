package main

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"github.com/shirou/gopsutil/process"
	"log"
	"net/http"
	"strconv"
	"time"
	"github.com/gobuffalo/packr"
	"os/exec"
	"runtime"
)

func main() {
	uiBox := packr.NewBox("./ui/dist")
	port := ":9000"
	http.Handle("/", http.FileServer(uiBox))
	http.HandleFunc("/getPsState", getPsState)
	http.HandleFunc("/terminate", terminate)
	go open("http://localhost:9000/")
	http.ListenAndServe(port, nil)
}

func open(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}

func terminate(w http.ResponseWriter, r *http.Request) {
	headers := http.Header{}
	headers.Set("Access-Control-Allow-Origin", "*")
	headers.Set("Access-Control-Allow-Credentials", "true")
	headers.Set("Access-Control-Request-Method", "*")
	upgrader := websocket.Upgrader{CheckOrigin: func(r *http.Request) bool {return true}}
	conn, err := upgrader.Upgrade(w, r, headers)
	if err != nil {
		log.Printf("Failed to set websocket upgrade: %+v", err)
		return
	}
	_, data, err := conn.ReadMessage()
	if err != nil {
		conn.WriteMessage(1, (&Response{Err:err.Error(), Data:nil}).ToJSON())
	}
	pid, err := strconv.Atoi(string(data))
	if err != nil {
		conn.WriteMessage(1, (&Response{Err:err.Error(), Data:nil}).ToJSON())
	}
	proc, err := process.NewProcess(int32(pid))
	if err != nil {
		conn.WriteMessage(1, (&Response{Err:err.Error(), Data:nil}).ToJSON())
	}
	err = proc.Terminate()
	if err != nil {
		conn.WriteMessage(1, (&Response{Err:err.Error(), Data:nil}).ToJSON())
	}
}

func getPsState(w http.ResponseWriter, r *http.Request) {
	headers := http.Header{}
	headers.Set("Access-Control-Allow-Origin", "*")
	headers.Set("Access-Control-Allow-Credentials", "true")
	headers.Set("Access-Control-Request-Method", "*")
	upgrader := websocket.Upgrader{CheckOrigin: func(r *http.Request) bool {return true}}
	conn, err := upgrader.Upgrade(w, r, headers)
	if err != nil {
		log.Printf("Failed to set websocket upgrade: %+v", err)
		return
	}
	ticker := time.NewTicker(8 * time.Second)
	for _ = range ticker.C {
		pids, err := process.Pids()
		if err != nil {
			conn.WriteMessage(1, (&Response{Err:err.Error(), Data:nil}).ToJSON())
		}
		resp := Response{Data:make([]Ps, 0)}
		for _, pid := range pids {
			proc, err := process.NewProcess(pid)
			if err != nil {
				continue
			}
			mem, err := proc.MemoryInfo()
			if err != nil {
				continue
			}
			cpu, err := proc.CPUPercent()
			if err != nil {
				continue
			}
			name, err := proc.Name()
			if err != nil {
				continue
			}
			ps := Ps{
				Pid: pid,
				Mem: strconv.FormatFloat(float64(float32(mem.RSS) / 1000000), 'f', 2, 64),
				Cpu: strconv.FormatFloat(cpu, 'f', 2, 64),
				Name: name,
			}
			resp.Data = append(resp.Data, ps)
		}
		conn.WriteMessage(1, resp.ToJSON())
	}
}

type Ps struct {
	Name string `json:"Name"`
	Pid int32 `json:"Pid"`
	Cpu string `json:"Cpu"`
	Mem string `json:"Mem"`
}

type Response struct {
	Err string
	Data []Ps
}

func (r *Response) ToJSON() []byte {
	b, _ := json.Marshal(r)
	return b
}