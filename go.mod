module bitbucket.org/uzdemir/systemMonitor

require (
	github.com/StackExchange/wmi v0.0.0-20180725035823-b12b22c5341f // indirect
	github.com/go-ole/go-ole v1.2.1 // indirect
	github.com/gobuffalo/packr v1.13.7
	github.com/gorilla/websocket v1.4.0
	github.com/shirou/gopsutil v2.17.12+incompatible
	github.com/shirou/w32 v0.0.0-20160930032740-bb4de0191aa4 // indirect
	golang.org/x/net v0.0.0-20180926154720-4dfa2610cdf3 // indirect
	golang.org/x/sys v0.0.0-20180909124046-d0be0721c37e // indirect
)
