import Vue from "vue";
import VueRecource from "vue-resource";
import App from "./App.vue";
import "./registerServiceWorker";

import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "babel-polyfill";

// import VueWebsocket from "vue-native-websocket";
//
Vue.config.productionTip = false;
Vue.use(Vuetify);
Vue.use(VueRecource);
// Vue.use(VueWebsocket, "ws://localhost:9000/tube", {
// 	origins: "*:*",
// });
new Vue({
	render: (h) => h(App),
}).$mount("#app");
